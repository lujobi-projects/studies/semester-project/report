% !TeX spellcheck = en_US
% !TeX encoding = UTF-8
% !TeX root = ../report.tex

\chapter{Introduction}
\label{chp:introduction}


\section{Motivation}
%\todo[inline]{motivate the importance of the work. Start broad (robots in the world, why are they important), what are the challenges (safety for ubiquitous deployment and so on) and then go more specific to this work (here we consider this aspect of the problem...)}
Fully autonomous cars have been envisioned since before the invention of computers. Thanks to the development in control, computation, and sensors of the past decades, we are getting closer to said vision. The wide use of autonomous vehicles promises to solve various current traffic issues (e.g., reducing jams, accidents, noise). However, it faces various challenges: unpredictable traffic, a very dynamic environment, and - especially at high speeds - a highly nonlinear system model. 

Despite the technological development, autonomous vehicle platforms remain expensive, especially the ones capable of racing. In order to provide students with hands-on experience and offer competition on various robotic platforms, the IDSC (Institute for Dynamics System and Control) at ETH Zurich, together with the Duckietown Foundation \cite{duckietown} developed \textbf{AIDO} \cite{AIDO}, the AI Driving Olympics. Students develop controllers or even complete autonomous control pipelines on simulators during this competition and submit them for evaluation. These submissions are then run on the actual system, evaluated, and scored. So far, this competition has been run on Duckiebots \cite{duckietown}, a small, differentially driven low-cost robot including a state-of-the-art software stack. However, plans are to extend the competition for the IDSC-gokart platform \cite{idscgokarts}, a much more expensive and way bigger vehicle running at high velocities. Collisions will have a severe impact and are potentially dangerous to humans and material. Thus, we need to avoid them whenever possible, or - if inevitable - their impact must be reduced as much as possible. 

Running untrusted controllers - submitted by students in case of AIDO - strengthens the need for a system avoiding collisions that can work with any type of controller. This piece of software will be called Guardian Angel. The same piece of software will find its usage in (machine) learning-based application where the ``classical'' approach of including an additional collision avoidance layer in the Controller directly (as in \cite{Liniger}) is not possible or an additional safety layer is required (see approach in \cite{tearle2021predictive}).

\section{Related Works} \label{sec:Related_work}
The problem of motion planning consists of finding a collision-free trajectory that is optimal with respect to some metrics. Nevertheless, often an agent is paired with a parallel safety system that is supposed to intervene in an emergency.
Intervention is necessary for two main reasons: 1) If something unexpected happens, the reaction time of the agent (perception, re-planning, and control) might be too high; 2) In some context (e.g., AIDO), it is not possible to fully ``trust'' the agent with its commands. Here the agent is treated as a black box paired with the Guardian Angel, which is supposed to take over when an unsafe situation is detected.

The literature proposes various methods to tackle the issue of supervising an agent and intervening.

Already in 1998, \cite{simplex} suggested running different controllers in parallel and use a decision logic to decide which controller-output to use. Based on a scoring of the current controller, the decision logic switches between experimental and established, trustworthy controllers. \cite{wabersich2018predictive} extended this concept to a \textit{safety filter}, applying the decision logic to every command before entering the physical system. This filter can override a given command should it be required to guarantee a save trajectory, essentially converting the unsafe system (model and controller) into a safe one, independent of the controller. 

\textbf{Ben Tearle et al.} \cite{tearle2021predictive}, show an approach to use an MPC-based \textit{predictive safety filter}, computing a positive semi-definite terminal set, to cover all possible inputs within a prediction horizon and certifying them as safe or not. Furthermore, they display how to use this filter for human-operated model cars and its application in training neural networks. Formulating the safety filter as an MPC problem is highly computationally demanding, and it is not well-posed for a dynamic environment. If - due to some external factors (e.g., other agents) - a collision is inevitable, the problem turns infeasible.

\textbf{Alexander Liniger} \cite{Liniger}, uses \textit{Viability Theory} in order to ensure a safe trajectory at any point in time and proof the existence thereof for a - theoretically - infinitely long time horizon. Using a kernel, he restricts the set of feasible states (for an MPC) at any point in time to retrieve only safe inputs. He extends said kernel to provide robustness against model uncertainties and discretization errors. These Kernels provide an efficient way to evaluate many trajectories online and offline. A similar approach is also prosecuted by \cite{ConvexOptimization} by using \textit{Convex Optimization} instead of Viability Theory. As in \cite{tearle2021predictive}, \cite{Liniger} assumes a static environment in his problem formulation and assumes the existence of a safe exit strategy in any reached state. Again, due to the assumption of a static environment, this approach does not address any external non-static obstacles (e.g., humans on the track).

In literature (e.g., \cite{ga_override_driver}), the concept of a Guardian Angel often refers to a system supervising (and supporting) the human in a vehicle that intervenes, should the average human reaction time not suffice to prevent a collision. We follow a similar approach with a new layer in the control structure supervising the agent. In contrast to \cite{tearle2021predictive} and \cite{Liniger} we want to extend the said layer to provide an interface that can accommodate the predictive capabilities - and thus the planned inputs - of the controller.

\section{Contribution}
In this work, we propose a Guardian Angel system that gets paired with the main agent to keep operations as safe as possible at any time, in any circumstance.
We treat the agent as a black box, from which we can read the desired commands which it intends to apply to the robot. If the evaluation proves these commands unsafe, the Guardian Angel is responsible for intervening with a better option.

Compared to related work (see \cite{Liniger}, \cite{tearle2021predictive}, \cite{wabersich2018predictive}) which mainly consider a static environment, we want our problem to be well defined in \emph{any} circumstance. If a collision is unavoidable because of external factors that are not under our control, we still want to find the least-worst solution. The principle of minimum violation planning \cite{minplanning_tumova} inspired our approach.
At the same time, we want the GA to be computationally ``lightweight'' to run in parallel to the main system without hitting the performance.

In order to achieve this, we propose to use a Guardian Angel Layer as a safety filter (similar to \cite{tearle2021predictive}, \cite{wabersich2018predictive}) that can intervene with the best possible trajectory at any time and that can be run with any controller. In contrast to \cite{tearle2021predictive}, said layer uses a finite set of predetermined input sequences - so-called scenarios - to evaluate predicted trajectories for the current state given the current controller input. The safety of each trajectory is determined based on the following factors: a nonlinear model of the robot, a representation of obstacles, and a cost function. Based on these costs, the Guardian Angel decides whether and how to intervene.

Considering the predictive capabilities of most controllers, we then extend the Guardian Angel further by an interface to the agent to communicate future planned commands. A system of trust determines how conservative the Guardian Angel takes these commands into account. The agent gains and loses trust again based on another cost function taking its volatility, safety margins, and further factors into account.

Finally, we showcase the proposed approach on an autonomous gokart platform with dynamic obstacles and untrusted controllers. 

%This thesis approaches the problem of keeping a safe escape trajectory similarly to Liniger's \cite{Liniger} approach in his Ph.D. Thesis. However, to evaluate the trajectories in real-time, the concept of the Viability Kernel has been simplified to the evaluation of a finite, predetermined set of inputs. Furthermore, Liniger \cite{Liniger} uses his approach during the path-planning operation of the actual controller. While, during this thesis, it will be applied to Guardian Angel Layer acting between the controller and the robot.

%This Guardian Angel has the advantage, compared to one interacting with humans, that it has the chance to read the requested controller inputs just in time and doesn't depend on measuring the first. 

\subsection{Manuscript Organization}

\begin{itemize}
    \item \textbf{Guardian Angel Layer}, in this chapter, the general idea behind the Guardian Angel and its integration with the system are explained in detail.
    \item \textbf{Guardian Angel via Finite Trajectory Set Evaluation} We explore the capabilities and explain the inner workings of a safety filter using a set of predetermined inputs. We predict a finite set of trajectories which are a scored. Based on this score the safety of the current input is deduced and - if needed - this input is overridden with a safer alternative guaranteeing recursive feasibility in a static environment. 
    \item \textbf{Guardian Angel with Agent's plans} Through a clever extension we provide an interface for the Agent to communicate its planned inputs. These planned inputs extend the set of predetermined inputs without altering the base functionality. Including the Agent's plan prevents some false interventions. A trust system is implemented in order to decide on how much we trust said plan.
    \item \textbf{Occupancy Grid}, in order for the Guardian Angel to react to changes in the environment quickly, some adjustments to the current obstacle detection had to be made.
\end{itemize}

~\Cref{sec:math_explained} explains some of the mathematical notation; all other symbols are explained when used first.


\subsection{Abbreviations, Notation and Mathematical Symbols}\label{sec:math_explained}

\begin{tabbing}
 \hspace*{7cm}  \= \kill
 GA \> Guardian Angel\\[0.5ex]
 MPC \> Model Predictive Control\\[0.5ex]
 MPCC \> Model Predictive Contouring Control\\[0.5ex]
 AIDO \> AI Driving Olympics\\[0.5ex]
 SLAM \> Simultaneous localization and mapping\\[0.5ex]
 RMSE \> root-mean-square error\\[0.5ex]
 

\end{tabbing}

\begin{tabbing}
 \hspace*{7cm}  \= \kill
 State \> $\gokartState$ \\[0.5ex]
 Initial State (at $t_0$) \> $\gokartStateInit = \gokartState(t_0)$ \\[0.5ex]
 Set of states (trajectory) \> $\gokartStates = \gokartStateSetTime $\\\>$= \gokartStateSet$ \\[0.5ex]
   
 Position (in world Frame) \> $\gokartPosition$ \\[0.5ex]
%  Function extracting position from state \> $\extractPosition: \gokartState \mapsto \gokartPosition$\\[0.5ex]
 Velocity (in gokart-Frame)\> $\gokartVelocity$ \\[0.5ex]
%  Function extracting velocity from state \> $\extractVelocity: \gokartState \mapsto \gokartVelocity$\\[0.5ex]
 
 Input \> $\gokartInput$ \\[0.5ex]
 Set of inputs\> $\gokartInputs = \gokartInputSetTime $\\\>$ = \gokartInputSet $\\[0.5ex]
 
\end{tabbing}
 All other mathematical symbols are explained when they are used for the first time in the thesis. 