% !TeX spellcheck = en_US
% !TeX encoding = UTF-8
% !TeX root = ../report.tex

\chapter{Guardian Angel with Agent's Plans}
\label{chp:ga_with_plan}

Many controllers use a predictive approach and know the estimated future commands they want to execute several steps in advance. Using these predicted commands, a state further in the future (than received by just evaluating the current command) can be estimated. The scenarios will then be executed from there on, minimizing the chances of a false intervention. To achieve this, the Guardian Angel provides an interface to which the Agent can send its \textit{commands plan}. 

\textbf{The base function of the Guardian Angel remains unchanged} - it still reacts to the instantaneous commands as well. We simply provide further scenarios to the set of scenarios, based on the plans of the Agent. To keep the basic working functionality is important should some commands not correspond to the plan or if the plan got too much outdated. Should a controller not want to submit its plan (e.g., a straightforward (PID) controller without horizon), the Guardian Angel's working principles remains consistent with the one described in \Cref{chp:ga_discrete_traj_set}.

\begin{figure}[ht]
\centering
% \subimport{tikz}{Guardian_Angel_Plus.tikz}
    \input{tikz/Guardian_Angel_Plus.tex}
\caption{Guardian Angel, Agent and Robot interplay with }
\label{fig:guardianAngel}
\end{figure}

\newpage

%%% Mathematical Terms
% Dimension of analysis: What does trust/conservative mean? 
% conservative -> intervenes more often
% trust -> what does trus affect/when to intervene
% comb of: systematically wrong predictions, volatile (changes predictions very often) 
% interface, transformations from now to pred state, inputs

% move "trust manger" into Guardian Angel




\section{Trusting the Controller}
Since most of the controllers used in the gokart have some predictive capabilities, it would be a reasonable approach to include more than one command of the controller before applying the static scenarios. This comes with the drawback that we need to rely to a certain degree on the Agent and its plan.

For that, two new terms will be introduced: \textit{Trust} and \textit{Conservativeness} of the Guardian Angel. The intuitive link between those is as follows: \textit{The more the Guardian Angel trusts the controller, the less conservative it is.} An mathematical definition will follow in the following sections.

\subsection{Trust}
The level of trust $\trust \in [0, 1]$ defines how much to \textit{trust} the controller $\trust = 1$ meaning that the controller is seen as completely trustworthy with respect to a metric $\trustMetric$ described below. Trustworthiness itself is the minimum over all trust metrics. 

\begin{align}
    \trust = \argmin_i \left(\trustMetric{_i}\left(\gokartInputs_{\mathrm{Controller}}, \gokartInputs_{\mathrm{prev}}, \costJ\left(\trajectory(\gokartState_0)\right), \gokartState, \dots \right) \right)
\end{align}

Similarly to scoring functions (see \Cref{subs:scoring_fct}), the trust metric scores a certain aspect of a the controller prediction on a scale from 0 to 1. Again, activation functions $\activationFunction$ (see Appendix \ref{app:activationfcts}) of neural networks prove to be useful in order to cap the the level of trust.

A list of ideas for the factors determining trust:
\begin{itemize}
    \item Stability or volatility of the Agent's plan:
        \begin{align}
            \trustMetric(\dots) = \activationFunction\left(\left|\gokartInputs_{\mathrm{Controller}}\left[0:N-1\right]-\gokartInputs_{\mathrm{prev}}\left[1:N\right]\right|\right)
        \end{align}
    \item Safety of the predicted trajectory, safety margins
        \begin{align}
            \trustMetric(\dots) = \activationFunction\left(\scoringFunction\left(\trajectoryCons\right)\right)
        \end{align}
    \item Reaction to newly occurring obstacles in the occupancy grid. 
    \item Quality of Controller state prediction (would need further extensions to the Agent)
\end{itemize}


\subsection{Hysteresis of Trust}
In most cases of the trust metrics, trust has to be built up over a longer span of time, but can be lost very quickly, e.g. the planned controller suddenly yields an unsafe trajectory. This is modelled as a asymmetric hysteresis with exponential smoothing on the rising side and a simple $\argmin$ on the falling side. The decay-factor $\alpha$ can be determined separately for every metric.
\begin{align}
    \trust_{t} &= \begin{cases}
        \trust_{t}^*  & \text{if $\trust_{t}^* < \trust_{t}$}\\
        \alpha\trust_{t}^* + (1-\alpha) \trust_{t-1}    & \text{else}
    \end{cases}
\end{align}

\subsection{Conservativeness of the Guardian Angel}
A more conservative Guardian Angel uses less commands of the controller before applying the precalculated scenarios thus intervenes more often. A lower bound is to only apply the direct controller-input $\gokartInput_0$, the upper bound (for a Controller with Horizon $N$, and predicted inputs $\gokartInputSet$) is to trust the controller inputs up to $\gokartInput_N$. Thus a very simple controller without any prediction horizon (like a simple PID controller) won't use this feature at all.

In other words the conservativeness $\conservativeness$ is defined through a function $\conservativenessFunction$ of the level of trust $\trust$ and the prediction horizon $N$ that defines the how many of the predicted Controller commands will be trusted to follow a safe trajectory. Again an activation function $\activationFunction$ will be applied to the level of trust.

\begin{align}
    \conservativeness &= \conservativenessFunction(N, \trust) = \lfloor N \cdot \activationFunction(\trust) \rfloor, \conservativeness \leq N\\
    \gokartInputsCons &= \gokartInputConsSet\\
    &\Longrightarrow \gokartInputsCons \subseteq \gokartInputsCtrl
\end{align}


The set of trusted Commands $\gokartInputsCons$ is the subset of all planned inputs $\gokartInputsCtrl$ containing the first $\conservativeness$ commands.

\subsection{Interplay between Trust and Conservativeness}
Equations \ref{eq:controller_state_scen} and \ref{eq:controller_traj_scen} will be modified in the following way:
\begin{alignat}{3}
    &\trajectoryCons &&= \gokartStateConsSet = \predFunc\left( \gokartStateInit, \gokartInputsCons\right) \label{eq:ctrl_tray_plan}\\
    &\trajectoryCtrlSetNew &&= \trajectoryCtrlSet \cup \left\{\trajectoryCons \cup \predFuncScen\left( \gokartState_{\conservativeness}, \gokartInput_{\conservativeness}, \scenFunc, d\right) \forall \scenFunc, \gokartDelay \in (\setS \times \gokartDelays) \right\}
\end{alignat}

Instead of just calculating the state $\gokartStateCtrl$ we calculate the part of the controller trajectory $\trajectoryCons$ corresponding to all inputs of the controller we trust $\gokartInputsCons$. The scenarios are then evaluated starting from the last state of this trajectory. The overall trajectory equals to the $\trajectoryCons$ united with the respective trajectory of the scenario.

From here on the same scoring functions and the same intervention logic will be applied as in \Cref{chp:ga_discrete_traj_set}. With one small modification: the trust $\trust$ gets reset to zero should the only safe trajectory lie in $\trajectoryCtrlSet$. Since this means that the controller selected an unsafe trajectory.

\section{Commands plan via ROS}
In order to determine planned commands, the controller has to "{}register"{} itself in the Guardian Angel. In order to communicate said plan, an interface has been determined in the form of two ROS messages:

\begin{verbatim}
PlannedCmd:
    # Motor Commands normalized, [-1, 1]
    float32 motor_left_cmd
    float32 motor_right_cmd
    
    # Brakes Commands normalized, [0, 1]
    float32 brake_cmd
    
    # Angle of the steering column reference [rad]
    float32 steering_ref_angle
    
    # Planned execution time of this command
    builtin_interfaces/Time timestamp

CommandsPlan:
    Header
    PlannedCmd[] commands
\end{verbatim}


\section{Results}
The same Scenarios as in \Cref{sec:disc_res} are applied, this time to the state after several controller inputs $\gokartState_{\conservativeness}$. This is visible in \Cref{fig:ga_with_plans_res}. And enables now the safety filter to be extended and use the trajectory of the controller for a certain distance.

\begin{figure}[h]
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[height=.7\textwidth]{images/ga_with_plans/predictive.png}
    \caption{Guardian Angel with predictions at $\trust = 0.3$}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.5\textwidth}
    \centering
    \includegraphics[height=.7\textwidth, trim={0cm 0 0 0}, clip]{images/ga_with_plans/predictive_zoom.png}
    \caption{GA with predictions at $\trust = 0.3$, zoomed in}
  \end{subfigure}
%   \medskip
%   \begin{subfigure}[t]{.5\textwidth}
%     \centering
%     \includegraphics[height=.7\textwidth]{images/ga_output/right_full.png}
%     \caption{Accelerating out of the corner}
%     \label{fig:ga_output_right_full}
%   \end{subfigure}
%   \hfill
%   \begin{subfigure}[t]{.5\textwidth}
%     \centering
%     \includegraphics[height=.7\textwidth]{images/ga_output/right_zoom.png}
%     \caption{Accelerating out of the corner, zoomed in}
%     \label{fig:ga_output_right_zoom}
%   \end{subfigure}
  \caption{Scenarios from \Cref{sec:disc_res} used in the Guardian Angel with an MPC-Controller}
  \label{fig:ga_with_plans_res}
\end{figure}

The output of the Guardian angel is further extended by the current level of trust:
\begin{verbatim}
[predictions_node-1] Level of trust: 0.3
\end{verbatim}

The calculation time is only minimally affected, as \Cref{eq:ctrl_tray_plan} has to be evaluate (instead of simulating $\gokartStateCtrl$) and the resulting trajectory has to be scored. This increase in calculation time is constant and about $0.3$ms with an standard deviation of $0.05$ms in the same setting as in \Cref{subsec:calc_time}. This is because we only score the second half of the trajectories $\trajectoryCtrlSet$ if no trajectory of the set $\trajectoryCtrlSetNew \setminus \trajectoryCtrlSet$ is safe.

