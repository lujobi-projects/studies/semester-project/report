% !TeX spellcheck = en_US
% !TeX encoding = UTF-8
% !TeX root = ../report.tex
\chapter{Occupancy Grid}
\label{chp:OccupancyGridChapter}
The occupancy grid creates a map of obstacles based on point cloud measurements from the lidar. Currently, it is in use while mapping, in order to then create the splines defining the track. So far, this was usually done in an offline configuration. Since we want to use it outside of mapping and online, some further changes were necessary apart from optimizing the code. An explanation follows in this chapter.

\begin{figure}[h]
    \centering
    \includegraphics[width=.8\textwidth]{images/occupancy_grid/with_extraction.png}
    \caption{Occupancy grid with track extraction, gray: dilated areas}
    \label{fig:occupancy_gridwith_extraction}
\end{figure}

\section{Bayesian Grid Filter}
The idea behind the bayesian grid filter is to use a stochastic approach to determine whether a cell of the grid is free or occupied. This filter is applied to the existing occupancy grid with every new measurement (i.e., a new point cloud). Our assumption of a cell being an obstacle is then updated based on this filter. 

\subsection{Current Setup}
Let's denote $\posterior \in \realsab{a}{b}$ as the posterior assumption of the grid and $\measObs \in \boolsab{c}{d}$ as the new measured obstacles. $\odds_\alpha$ denotes the odds that the lidar hits when the cell has an obstacle, $\odds_\beta$ the odds that the lidar misses even though it has an obstacle. The output is defined as $y \in \boolsab{c}{d}$, true meaning that an obstacle has been detected.

\begin{align}
    \posterior_0 &= 0 \in \realsab{a}{b}\\
    u_k &= \begin{cases}
        \odds_\alpha   & \text{if $\measObs$ is $\text{True}$}\\
        \odds_\beta    & \text{else}
    \end{cases}\\
    \posterior_k &= u_k + \posterior_{k-1}\\
    y_k &= \begin{cases}
        \text{True}   & \text{if $\posterior_{k} > \text{threshold}$}\\
        \text{False}  & \text{else}
    \end{cases}\label{eqn:BayesianGridFilter_current}
\end{align}

The Equation \ref{eqn:BayesianGridFilter_current} has one shortcoming: after having seen the track "{}for long enough"{} newly added obstacles (especially moving ones) have a hard time appearing in $y_{k}$. E.g. a person walking over the track is not seen as obstacle after the gokart has completed a lap.

\subsection{Adjustments} \label{sec:occ_grid_adj}
After completing a lap (driven manually or at very low velocity) we fix an obstacle mask which - from then on - is always counted as obstacle, denoted as $y_F$.
Additionally the influence of $\posterior_k$ in calculating $\posterior_{k+1}$ can be reduced by multiplying it with a factor $\delta \in[0, 1]$. This lets the older measurements, which are not tracked anymore, decay over time and guarantees to always keep track of the track borders. However, this approach assumes that the track remains stationary, and it is, for example, (still) not possible to open up some borders of the track after $y_F$ has been fixed. 

\begin{align}\label{eqn:BayesianGridFilter_prop}
    \posterior_{k} &= u_k + \delta\cdot\posterior_{k-1}\\
    y_k &= y_F \cup \begin{cases}
        \text{True}   & \text{if $\posterior_{k} > \text{threshold}$}\\
        \text{False}  & \text{else}
    \end{cases}
\end{align}

Figure \ref{fig:occ_grid_decaying} shows the faster updating, decaying occupancy grid using the preprocessed obstacle mask from  \Cref{ssec:obs_mask_prep}. Taking a closer look at the \Cref{fig:occ_grid_dec_wo_o}, the obstacles are decaying some meters behind the gokart, even though it already completed several laps.

\begin{figure}[h]
  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{images/occupancy_grid/without_overlay.png}};
        \draw[red,ultra thick,rounded corners] (1.6,0.9) circle (0.02);
    \end{tikzpicture}
    %\includegraphics[width=\textwidth]{images/occupancy_grid/without_overlay.png}
    \caption{Without overlay}
    \label{fig:occ_grid_dec_wo_o}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{images/occupancy_grid/with_overlay.png}};
        \draw[red,ultra thick,rounded corners] (1.6,0.9) circle (0.02);
    \end{tikzpicture}
    \caption{With overlay}
  \end{subfigure}
  \caption{Decaying Occupancy Grid, red: approximate position of the gokart}
  \label{fig:occ_grid_decaying}
\end{figure}

\subsection{Preprocessing the Obstacle Mask} \label{ssec:obs_mask_prep}
In order to have a clean representation of the track, $y_F$ is preprocessed with various Morphological operations (See \Cref{app:morph}) in order to smooth the borders. Furthermore by labeling connected regions only the track is extracted.

\begin{figure}[h]
  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/occupancy_grid/orig.png}
    \caption{Original Occupancy Grid}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/occupancy_grid/after_closing.png}
    \caption{Grid after opening free space}
  \end{subfigure}

  \medskip

  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/occupancy_grid/labeled.png}
    \caption{Extracted mask}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{images/occupancy_grid/comparison.png}
    \caption{Difference to original, red: added obstacles, blue: seed for mask region}
  \end{subfigure}
\end{figure}


\section{Dilating Obstacles}
The size of the gokart (modeled as a circle) has to be taken into account since it cannot be modeled as a single point in the center of gravity of the gokart — otherwise, only intersections of the trajectory of the center of gravity and an obstacle count as collision. A possible solution is to either check an area around the trajectory or - the faster approach - to dilate all obstacles with a structuring element of the size of the gokart. An example of the dilation before the adjustments of \Cref{sec:occ_grid_adj} can be found in \Cref{fig:occupancy_gridwith_extraction}. Using the new obstacle mask, the dilation can be seen in \Cref{fig:occupancy_grid_erroded}.
\begin{figure}[ht]
    \centering
    \includegraphics{images/occupancy_grid/erroded.png}
    \caption{Dilated mask, gray: the dilated obstacles}
    \label{fig:occupancy_grid_erroded}
\end{figure}


